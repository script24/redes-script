#!/bin/bash

echo -e '\033[0;32m{>}\033[0m redireciona para saída' 
cat teste1.txt  > /tmp/teste
echo -e '\033[0;32m{>>}\033[0m redireciona para fim de arquivo'
cat teste3.txt >> teste
echo -e '\033[0;32m{<}\033[0m redireciona para a entrada'
cat < teste.txt
