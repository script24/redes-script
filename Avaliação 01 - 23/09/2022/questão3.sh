#!/bin/bash 
 
echo '$USER exibe o nome do usuário logado no shell' 
printenv USER 
 
echo '$PWD exibe o diretório atual, no caso onde o script está sendo executado' 
printenv PWD 
 
echo '$HOME exibe o diretório do usuário logado' 
printenv HOME 
 
echo '$SHELL exibe o shell utilizado' 
printenv SHELL 
 
echo '$OSTYPE exibe o tipo do sistema operacional' 
printenv OSTYPE 
 
echo '$RANDOM gera um número randômico e exibe na tela' 
printenv RANDOM 
 
echo '$DISPLAY exibe o nome do computador e o número do display' 
printenv DISPLAY 
 
echo '$EUID exibe o user ID de quem executou o script' 
printenv EUID 
 
echo '$PATH exibe o path do sistema atual' 
printenv PATH 
 
echo '$LANG exibe o idioma que o sistema está usando' 
printenv LANG 
 
echo 'TERM exibe o tipo de terminal em uso' 
printenv TERM 
 
echo 'LINES retorna o número de linhas do terminal' 
printenv LINES  
 
echo 'MACHTYPE obtem a arquitetura da máquina' 
printenv MACHTYPE 
 
echo 'GROUPS retorna informações sobre o GID' 
printenv GROUPS 
 
echo 'CDPATH caminho de busca do comando cd' 
printenv CDPATH
