#!/bin/bash

function menu() {
        echo "==============================="
        echo "[A] selecione um arquivo"
        echo "[2] mostrar preview do arquivo"
        echo "[B] exibir o arquivo selecionado"
        echo "[C] criar um arquivo com N linhas"
        echo "[D] Pesquise algo no arquivo selecionado"
        echo "[F] criar uma cópia do arquivo selecionado"
        echo "[S] sair"
        sleep 1

while [ "$opc" != "s" ]; do
        menu
        read -p "Digite a opção: " opc

        if [ "$opc" = "a" ]
        then
                read -p "Digite o nome do arquivo: " arq
                if [ -f $arq ]
                then
                        echo "$arq"
                else
                        echo "Nome inválido"
                fi
        elif [ "$opc" = "b" ]
        then
                head -2 $arq
        elif [ "$opc" = "c" ]
        then
                cat $arq
        elif [ "$opc" = "d" ]
        then
                read -p "Digite o numero de linhas: " n
                head -"$n" blablabla.txt > novoblablabla.txt
                echo "Exibindo o novo arquivo"
                cat novoblablabla.txt
        elif [ "$opc" = "e" ]
        then
                read -p "Digite a palavra pesquisada no arquivo selecionado: " palavra
                grep -R "$palavra" "$arq"
        elif [ "$opc" = "f" ]
        then
                cp "$arq" novo-arq.txt
        fi
done

