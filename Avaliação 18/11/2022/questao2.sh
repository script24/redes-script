#!/bin/bash

STRING=$1
sort -R $STRING &> /dev/null

KEY="chave.key"
QTDD=${#STRING}
function cripto() {
seq 1 $QTDD | while read CHAR; do
        BIT=$(echo $STRING | cut -c$CHAR)
        BIT_NEW=$(grep "$BIT=" $KEY | awk -F'=' '{print $2}')
        printf "$BIT_NEW"
done
echo
}

cripto $STRING
