# er = a ab [ab] F[Aa]B FA{1,5}B {} - contador [] - ou
# ls = a ab a,b FAB,Fab FAB,FAAB,....FAAAAAB

reget machine
reget learn
grep
usar o bash
 
 . = QUALQUER CARACTERE

 ^    $
{0,}  *
{1,}  +
{0,1} ?

FA*B -> FB 
        FAB, FAAB
        FAAAB...

F[AB]? -> F 
          FA
          FB

^.. = DOIS PRIMEIROS CARACTERES
.* = TODOS OS CARACTERES
..$ = DOIS ÚLTIMOS CARACTERES
^...$ =  LISTAS C TRES CARACTERES
^.{3,11}$ = O 1º CARACTER REPETIDO DE 3 à 11 vezes.
[^ ] = TUDO, EXCETO O QUE VEM DEPOIS, NESSE CASO, O ESPAÇO

# expressões regulares

# grep = filtra

head -5 /etc/passwd | grep --color 'a'
head -5 /etc/passwd | grep --color [abcdefghij] # [a-zA-Z]
head -5 /etc/passwd | grep -E --color '^[a-zA-Z]' #^ = começo da linha
head -5 /etc/passwd | grep -E --color '[a-zA-Z_0-9]{1,}'$ #$ = final da linhaa
head -5 /etc/passwd | grep -o -E --color '[a-zA-Z_0-9]{1,}$' #-o = exclui tudo que não estiver nas condições do grep
cat pru.txt | grep -E -o '[^ ]*$'

1º isolar os nomes

2º listar apenas o username

3º apenas letras maiusculas, apenas minusculas, numero e _. começa com letra minusculas e tem no mínimo 4 caracteres.


